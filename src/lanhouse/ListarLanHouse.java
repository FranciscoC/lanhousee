/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lanhouse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author aluno
 */
class ListarLanHouse {
    public void listar (JList ListaLanHouse) throws SQLException {
        ListaLanHouse.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = Conexao.ObterConexao();
        String SQL = "select from sc_LanHouse.cliente";
        PreparedStatement ps = c.prepareStatement(SQL);
        ResultSet rs = ps.executeQuery(SQL);
        while (rs.next()){
            dfm.addElement(rs.getString("nome"));
        }
        ListaLanHouse.setModel(dfm);
        c.close();
    }
    
    public void ListarCliente (JList ListaCliente) throws SQLException{
        ListaCliente.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = Conexao.ObterConexao();
        String SQL = "select from sc_LanHouse.cliente";
        PreparedStatement ps = c.prepareStatement(SQL);
        ResultSet rs = ps.executeQuery(SQL);
        while (rs.next()){
            dfm.addElement(rs.getString("nome"));
        }
        ListaCliente.setModel(dfm);
        c.close();
    }
    
    
}
