/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lanhouse;

/**
 *
 * @author aluno
 */
public class Computador {
    
    int numero;
    String marca, placadevideo, processador;

    public Computador(int numero, String marca, String placadevideo, String processador) {
        this.numero = numero;
        this.marca = marca;
        this.placadevideo = placadevideo;
        this.processador = processador;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlacadevideo() {
        return placadevideo;
    }

    public void setPlacadevideo(String placadevideo) {
        this.placadevideo = placadevideo;
    }

    public String getProcessador() {
        return processador;
    }

    public void setProcessador(String processador) {
        this.processador = processador;
    }
    
    
}
